#!/bin/sh

basename=`echo $1 | cut -d '.' -f1`

echo "Compilation de $1 en cours..."

latex ${basename}.tex -quiet >/dev/null
latex ${basename}.tex -quiet >/dev/null
dvips ${basename}.dvi -q >/dev/null
ps2pdf ${basename}.ps >/dev/null

rm -f ${basename}.dvi
rm -f ${basename}.aux
rm -f ${basename}.log
rm -f ${basename}.out
rm -f ${basename}.ps
