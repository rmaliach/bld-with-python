#!/usr/bin/python3
from os import system
from os import listdir 
from subprocess import call as call
file = open("liste_assos.txt","r",encoding="utf-8")
def genconv(listeasso):
        type=listeasso[0]
        for i in range(1,len(listeasso)):
                temp=listeasso[i].split('|')
                adr="Clauses/"+temp[3]
                try:
                        with open(adr,'r') as fichier:
                                clauses=fichier.read()
                except IOError:
                        clauses="Pas de clauses particulières"
                call(["./gen_file.py",type,temp[0],temp[1],temp[2],clauses])
lignes=file.readlines()
for i in range(len(lignes)):
        lignes[i]=lignes[i].rstrip('\n')
lignes = list(filter(None, lignes))
idcommission=lignes.index("COMMISSION")
idprojet=lignes.index("PROJET")
idclub=lignes.index("CLUB")
idasso=lignes.index("1901")

idtypes=[idcommission,idasso,idclub,idprojet]
idtypes.sort()
genconv(lignes[idtypes[0]:idtypes[1]])
genconv(lignes[idtypes[1]:idtypes[2]])
genconv(lignes[idtypes[2]:idtypes[3]])
genconv(lignes[idtypes[3]:])
files=listdir()
files = [file for file in files if file[-4:] =='.tex']
for file in files:
        call(["./compile.sh",file[:-4]])
        #print("./compile.sh "+file[:-4])
system("rm merged.pdf")
system("./concat_pdf.sh")
system("rm convention_*.pdf")
