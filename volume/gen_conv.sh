#!/bin/bash

fichier=$1
TYPE="COMMISSION"

cat $fichier | while read LINE
do
case "$LINE" in
	"") ;;
	"COMMISSION") TYPE="COMMISSION" ;;
	"PROJET") TYPE="PROJET" ;;
	"CLUB") TYPE="CLUB" ;;
	"1901") TYPE="1901" ;;
	*)	nomAsso=`echo $LINE | cut -d \| -f 1`
		mailAsso=`echo $LINE | cut -d \| -f 2`
		case "$TYPE" in
			"COMMISSION") objet=`echo $LINE | cut -d \| -f 3`
						clausesPart=`echo $LINE | cut -d \| -f 4`
						if [ "$clausesPart" = "" ]
						then
							clausesPart="(pas de clauses particulières)"
						fi
						./gen_file.sh "$TYPE" "$nomAsso" "$mailAsso" "$objet" "$clausesPart" ;;
			"PROJET") objet=`echo $LINE | cut -d \| -f 3`
						dateFin=`echo $LINE | cut -d \| -f 4`			
						clausesPart=`echo $LINE | cut -d \| -f 5`
						if [ "$clausesPart" = "" ]
						then
							clausesPart="(pas de clauses particulières)"
						fi 
						./gen_file.sh "$TYPE" "$nomAsso" "$mailAsso" "$objet" "$clausesPart" "$dateFin";;
			"CLUB") objet=`echo $LINE | cut -d \| -f 3`
						clausesPart=`echo $LINE | cut -d \| -f 4`
						if [ "$clausesPart" = "" ]
						then
							clausesPart="(pas de clauses particulières)"
						fi 
						./gen_file.sh "$TYPE" "$nomAsso" "$mailAsso" "$objet" "$clausesPart" ;;
			"1901") clausesPart=`echo $LINE | cut -d \| -f 3`
					if [ "$clausesPart" = "" ]
					then
						clausesPart="(pas de clauses particulières)"
					fi
					presentationAsso=`echo $LINE | cut -d \| -f 4`
					./gen_file.sh "$TYPE" "$nomAsso" "$mailAsso" "$clausesPart" "$presentationAsso" ;;
		esac;;
esac

done

read -n1 -r -p "Fichiers générés... Attente pour la compilation..." key

for compiler in *.tex; do
	./compile.sh "$compiler"
   cp "${compiler%.tex}.pdf" "${compiler%tex}_2.pdf"
done

rm merged.pdf
./concat_pdf.sh

rm convention_*.pdf
evince merged.pdf &
