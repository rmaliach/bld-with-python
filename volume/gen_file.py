#!/usr/bin/python3
from sys import argv as argv
from sys import exit as exit
import sys
from shutil import copy

if len(argv)<6:
    print("Not enough args")
    exit(1)
typeasso=argv[1]
nomasso=argv[2]
mailasso=argv[3]
desc=argv[4]
clauses=argv[5]
path1="convention_"+mailasso+".tex"
path2="convention_"+mailasso+"2.tex"
fichier1=open(path1, "w")
sys.stdout = fichier1
print("\\documentclass[a4paper]{article}")
if typeasso=="COMMISSION":
    print("\\usepackage[commission]{conventions}")
    print("\\objet{"+desc+"}")
elif typeasso=="CLUB":
    print("\\usepackage[club]{conventions}")
    print("\\objet{"+desc+"}")
elif typeasso=="1901":
    print("\\usepackage[asso]{conventions}")
    print("\\objet{"+desc+"}")
else:
    print("\\usepackage[projet]{conventions}")
    print("\\objet{"+desc+"}")
print("\\nomEntite{"+nomasso+"}")
print("\\mailAsso{"+mailasso+"}")
print("\\clausesPart{"+clauses+"}")
print("\\pole{Pôle Vie du Campus}\n\\poleMail{polevdc}\n\\poleSiret{809 856 537 00018}\n\\poleSigle{PVDC}\n\\dateVote{12 mars 2019}\n\\begin{document}\n\\printConvention{}\n\\end{document}")
#copystat(path1,path2)
fichier1.close()
copy(path1,path2)
