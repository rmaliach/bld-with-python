#!/usr/bin/python3
from os import system
from os import listdir 
from subprocess import call as call

def genconv(listeassos):
	typeasso=listeassos[0]
	for i in range(1,len(listeassos)):
		temp=listeassos[i].split('|')
		adr="Clauses/"+temp[3]
		try:
			with open(adr,'r') as fichier:
				clauses=fichier.read()
		except IOError:
				clauses="Pas de clauses particulières"
		
		call(["./gen_file.py",typeasso,temp[0],temp[1],temp[2],clauses])	

file = open("liste_assos.txt","r")
lignes=file.readlines()
for i in range(len(lignes)):
        lignes[i]=lignes[i].rstrip('\n')
lignes = list(filter(None, lignes)) 
print(lignes)
idcommission=lignes.index("COMMISSION")
idclub=lignes.index("CLUB")
idprojet=lignes.index("PROJET")
idasso=lignes.index("1901")
index=[idcommission,idclub,idprojet,idasso]
index.sort()
genconv(lignes[index[0]:index[1]])
genconv(lignes[index[1]:index[2]])
genconv(lignes[index[2]:index[3]])
genconv(lignes[index[3]:])
files=listdir('.')
print(files)
texfiles=[file for file in files if file[-4:] == '.tex']
print(texfiles)
for file in texfiles:
	call(["./compile.py",file])
system("rm merged.pdf")
system("./concat_pdf.sh")
system("rm convention_*.pdf")
