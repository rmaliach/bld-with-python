#!/usr/bin/python3
from subprocess import call as call
from sys import argv as argv

basename=argv[1]
basename=basename[0:-4]
basenametex=basename+'.tex'
basenamedvi=basename+'.dvi'
basenameps=basename+'.ps'
call(["latex",basenametex,"-quiet",">/dev/null"])
call(["latex",basenametex,"-quiet",">/dev/null"])
call(["dvips",basenamedvi,"-q",">/dev/null"])
call(["ps2pdf",basenameps,">/dev/null"])

system("rm -f "+basename+".dvi")
system("rm -f "+basename+".aux")
system("rm -f "+basename+".log")
system("rm -f "+basename+".out")
system("rm -f "+basename+".ps")

