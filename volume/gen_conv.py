#!/usr/bin/python3
from os import system
from os import listdir 
from subprocess import call as call
file = open("liste_assos.txt","r")

#Fonction qui réalise les appels systèmes
#Le tableau passé en paramètre contient comme premier élément le type
#On sépare les lignes suivantes en un tableau grâce au caractère | 
#On vérifie si un fichier clauses particulières existe, si oui on le lit dans le dossier Clauses. Sinon, on définit la string "Pas de clauses particulières"
#On appelle le script gen_file en lui passant en paramètre les différents éléments.

def genconv(listeasso):
        type=listeasso[0]
        for i in range(1,len(listeasso)):
                temp=listeasso[i].split('|')
                adr="Clauses/"+temp[3]
                try:
                        with open(adr,'r') as fichier:
                                clauses=fichier.read()
                except IOError:
                        clauses="Pas de clauses particulières"
                call(["./gen_file.py",type,temp[0],temp[1],temp[2],clauses])

#Lecture de toutes les lignes, élimination des \n et des lignes vides
#readlines() renvoie un tableau contenant toutes les lignes lues
#rstrip('\n') permet de se débarasser des caractères parasites \n
#filter(None,lignes) permet de se débarasser des lignes vides
lignes=file.readlines()
for i in range(len(lignes)):
        lignes[i]=lignes[i].rstrip('\n')
lignes = list(filter(None, lignes)) 

#Détermination des id dans le tableau
#On recherche l'index de chacun des types d'associations dans le tableau. Puis, on crée un tableau contenant ces index, et on les trie dans l'ordre croissant
idcommission=lignes.index("COMMISSION")
idprojet=lignes.index("PROJET")
idclub=lignes.index("CLUB")
idasso=lignes.index("1901")
idtypes=[idcommission,idasso,idclub,idprojet]
idtypes.sort()

#Generation des fichiers .tex
#Les lignes concernant chaque type sont situées entre l'id de ce type, et l'id du type suivant.
#On passe a la fonction genconv un sous-tableau de lignes, qui contient comme premier élément le type, puis les informations sur les associations 
genconv(lignes[idtypes[0]:idtypes[1]])
genconv(lignes[idtypes[1]:idtypes[2]])
genconv(lignes[idtypes[2]:idtypes[3]])
genconv(lignes[idtypes[3]:])

#On stocke tous les fichiers du répertoire courant dans un tableau de string
#On élimine de ce tableau tous les fichiers dont l'extension n'est pas ".tex"
#On appelle le script compile.sh sur chacun des fichiers .tex
files=listdir()
files = [file for file in files if file[-4:] =='.tex']
for file in files:
        call(["./compile.sh",file[:-4]])
        #print("./compile.sh "+file[:-4])
system("rm merged.pdf")
system("./concat_pdf.sh")
system("rm convention_*.pdf")
