#!/bin/bash

TYPE=$1
nomAsso=$2
mailAsso=$3
filename="convention_${mailAsso//.}.tex"
filename2="convention_${mailAsso//.}_2.tex"
echo "\documentclass[a4paper]{article}" > $filename

touch $filename
case "$TYPE" in
	"COMMISSION") objet=$4
				clausesPart=$5
				echo "\usepackage[commission]{conventions}
				
\objet{$objet}" >> $filename
				;;
	"PROJET") objet=$4
			clausesPart=$5
			dateFin=$6
			echo "\usepackage[projet]{conventions}
			
\objet{$objet}
\dateFinProj{$dateFin}" >> $filename
				;;
	"CLUB") objet=$4
				clausesPart=$5
			echo "\usepackage[club]{conventions}
			
\objet{$objet}" >> $filename
				;;
	"1901") clausesPart=$4
			presentationAsso=$5
			echo "\usepackage[asso]{conventions}
			
\presentationAsso{$presentationAsso}
\objet{}" >> $filename

				;;
esac


echo "\nomEntite{$nomAsso}
\mailAsso{$mailAsso}
\clausesPart{$clausesPart}

\pole{Pôle Vie du Campus}
\poleMail{polevdc}
\poleSiret{809 856 537 00018}
\poleSigle{PVDC}
\dateVote{6 janvier 2020}

\begin{document}
\printConvention{}
\end{document}" >> $filename

#cp $filename $filename2
echo "fichier $filename généré."
